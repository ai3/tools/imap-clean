package main

import (
	"encoding/json"

	"github.com/emersion/go-sasl"
	"github.com/sqs/go-xoauth2"
)

const XOAuth2 = "XOAUTH2"

type xoauth2Client struct {
	username, token string
}

func newXOAuth2Client(username, token string) *xoauth2Client {
	return &xoauth2Client{
		username: username,
		token:    token,
	}
}

func (c *xoauth2Client) Start() (mech string, ir []byte, err error) {
	authzid := xoauth2.OAuth2String(c.username, c.token)
	return XOAuth2, []byte(authzid), nil
}

func (c *xoauth2Client) Next(challenge []byte) ([]byte, error) {
	var authBearerErr sasl.OAuthBearerError
	if err := json.Unmarshal(challenge, &authBearerErr); err != nil {
		return nil, err
	}
	return nil, &authBearerErr
}
