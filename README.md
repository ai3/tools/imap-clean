# imap-clean

Simple tool to remove old messages from a remote mailbox over IMAP
(including GMail). Its intended usage is being run periodically from a
cron job to control mailbox usage of test accounts used by
[ai3/tools/service-prober](https://git.autistici.org/ai3/tools/service-prober)
and similar tools.

## Installation

To build and install the *imap-clean* binary, run:

```shell
go install git.autistici.org/ai3/tools/imap-clean@latest
```

## Usage

The tool will scan the remote mailbox and delete messages older than
the number of days provided by the *--days* option (default is 15).

Use the *--server* command-line option to specify which server to
connect to (in *host:port* format): the tool will automatically figure
out if it needs to talk TLS or STARTTLS, unencrypted connections are
not supported.

Authentication is controlled by the *--username* and *--password*
options. The tool also supports XOAUTH2 SASL authentication (used by
GMail, for instance), in which case, instead of the *--password*
option you should use *--bearer-token* with the appropriate OAuth2
token.
