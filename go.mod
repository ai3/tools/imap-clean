module git.autistici.org/ai3/tools/imap-clean

go 1.15

require (
	github.com/emersion/go-imap v1.2.1
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21
	github.com/sqs/go-xoauth2 v0.0.0-20120917012134-0911dad68e56
)
